import React from 'react'
import Component from './index'
import 'bootstrap/dist/css/bootstrap.min.css';

export default function MyApp({ Component, pageProps }) {
    return <Component {...pageProps} />
}
