import React from 'react'
import Header from "../components/Header";
import SubHeader from "../components/SubHeader";
import Footer from "../components/Footer/Footer";
import Main from "../components/Main";
import Review from "../components/Main/Review";
import Networking from "../components/Main/Networking";
import Teams from "../components/Main/Teams";

import styled from 'styled-components'

const Container = styled.div`
    display: flex;
    flex-direction: column;
`

const Home = () => {
  return (
      <Container>
          <Header/>
          <SubHeader />
          <Main />
          <Review />
          <Networking />
          <Teams />
          <Footer />
      </Container>
  )
}

export default Home
