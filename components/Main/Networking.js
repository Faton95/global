import React, { useState } from 'react';
import { TabContent, TabPane, Nav, NavItem, NavLink, Container, Row, Col } from 'reactstrap';
import { Layout } from '../UI/Layout'
import styled from "styled-components";

const StyledNav = styled(Nav)`
    border: none;
`
const StyledNavLink = styled(NavLink)`
    background-color: transparent;
    border: none !important;
    float: right;
    margin: 5px;
    cursor: pointer;
    border-radius: 4px;
`

const StyledColLeft = styled(Col)`
    background-image: linear-gradient(rgba(3,28,58,.8), rgba(3,28,58,.8)), url(${'/hands.jpg'}) !important;
    z-index: 200;
    color: #fff;
    text-align: right;
    padding-right: 45px;
    padding-top: 80px;
    padding-bottom: 80px;
    min-height: 560px;
    position: relative;
    z-index: 10;
    background-size: cover;
`
const StyledColRight = styled(Col)`
    background-color: #eff5fd !important;
`
const StyledRow = styled(Row)`
    margin: 0 -15px;
`
const IMG = styled.img`
    max-width: 25%;
    flex: 0 0 25%;
    margin: 25px 20px;
`
const Networking = (props) => {
    const [activeTab, setActiveTab] = useState('1');

    const toggle = tab => {
        if(activeTab !== tab) setActiveTab(tab);
    }

    return (
        <Layout>
            <Container>
            <StyledRow>
                <StyledColLeft sm="4">
                    <StyledNav tabs vertical>
                        <h1>Networking</h1>
                        <NavItem>
                            <StyledNavLink
                                active={activeTab === '1'}
                                onClick={() => { toggle('1'); }}
                            >
                                Mobile Operators
                            </StyledNavLink>
                        </NavItem>
                        <NavItem>
                            <StyledNavLink
                                active = {activeTab === '2'}
                                onClick={() => { toggle('2'); }}
                            >
                                Banking
                            </StyledNavLink>
                        </NavItem>
                        <NavItem>
                            <StyledNavLink
                                active = {activeTab === '3'}
                                onClick={() => { toggle('3'); }}
                            >
                                Government
                            </StyledNavLink>
                        </NavItem>
                        <NavItem>
                            <StyledNavLink
                                active = {activeTab === '4'}
                                onClick={() => { toggle('4'); }}
                            >
                                Others
                            </StyledNavLink>
                        </NavItem>
                    </StyledNav>
                </StyledColLeft>
                <StyledColRight sm="8">
                    <TabContent activeTab={activeTab}>
                        <TabPane tabId="1">
                            <IMG src="/logos/1.png" alt="1"/>
                            <IMG src="/logos/2.png" alt="1"/>
                            <IMG src="/logos/3.png" alt="1"/>
                            <IMG src="/logos/4.png" alt="1"/>
                            <IMG src="/logos/5.png" alt="1"/>
                            <IMG src="/logos/6.png" alt="1"/>
                            <IMG src="/logos/7.png" alt="1"/>
                            <IMG src="/logos/8.png" alt="1"/>
                        </TabPane>
                        <TabPane tabId="2">
                            <IMG src="/logos/3.png" alt="1"/>
                            <IMG src="/logos/5.png" alt="1"/>
                            <IMG src="/logos/8.png" alt="1"/>
                            <IMG src="/logos/2.png" alt="1"/>
                            <IMG src="/logos/7.png" alt="1"/>
                            <IMG src="/logos/1.png" alt="1"/>
                            <IMG src="/logos/4.png" alt="1"/>
                            <IMG src="/logos/6.png" alt="1"/>
                        </TabPane>
                        <TabPane tabId="3">
                            <IMG src="/logos/6.png" alt="1"/>
                            <IMG src="/logos/7.png" alt="1"/>
                            <IMG src="/logos/8.png" alt="1"/>
                            <IMG src="/logos/5.png" alt="1"/>
                            <IMG src="/logos/4.png" alt="1"/>
                            <IMG src="/logos/2.png" alt="1"/>
                            <IMG src="/logos/3.png" alt="1"/>
                            <IMG src="/logos/1.png" alt="1"/>
                        </TabPane>
                        <TabPane tabId="4">
                            <IMG src="/logos/5.png" alt="1"/>
                            <IMG src="/logos/1.png" alt="1"/>
                            <IMG src="/logos/2.png" alt="1"/>
                            <IMG src="/logos/4.png" alt="1"/>
                            <IMG src="/logos/8.png" alt="1"/>
                            <IMG src="/logos/3.png" alt="1"/>
                            <IMG src="/logos/6.png" alt="1"/>
                            <IMG src="/logos/7.png" alt="1"/>
                        </TabPane>
                    </TabContent>
                </StyledColRight>
            </StyledRow>
            </Container>
        </Layout>
    );
}

export default Networking;
