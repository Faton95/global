import React from 'react'
import styled from 'styled-components'
import {Container, Row, Col} from 'reactstrap'
import Button from "../UI/Button";

const HeaderBlock = styled.div`
    margin: 50px auto;
    background-color: #072344 !important;
    height: 200px;
    display: flex;
    justify-content: space-between;
    align-items: flex-end;
    padding: 20px;
    width: 100%;
`
const BodyBlock = styled.div`
    display: flex;
    justify-content: space-between;
    flex-wrap: wrap;
    margin-bottom: 40px;
`
const H1 = styled.h1`
    color: white;
`
const ImgBlock = styled.div`
    max-width: 150px;
    max-height: 150px;
    position: relative;
    display: inline-block;
    z-index: 9999;
    margin-bottom: 10px;
`
const ImgText = styled.div`
    position: absolute;
    z-index: 999;
    left: 5%;
    bottom: 1%;
    width: 60%; 
    color: white;
    
`
const Img = styled.img`
    height: 100%;
    box-shadow: inset 50px 50px 10px red;
`

const H6 = styled.h6`
    clear: both;
    display: inline-block;
    overflow: hidden;
    white-space: nowrap;
`
const Teams = () => {
    return(
        <Container>
            <HeaderBlock>
                    <H1>Team of Professionals</H1>
                    <Button name={'+ Join us'}/>
            </HeaderBlock>
            <BodyBlock>
                <ImgBlock>
                    <Img src="/avatars/2.jpg" alt="123" />
                    <ImgText>
                        <h5>Kamila</h5>
                        <H6>Product manager</H6>
                    </ImgText>
                </ImgBlock>
                <ImgBlock>
                    <Img src="/avatars/1.jpg" alt="123" />
                    <ImgText>
                        <h5>Kamila</h5>
                        <H6>Product manager</H6>
                    </ImgText>
                </ImgBlock>
                <ImgBlock>
                    <Img src="/avatars/5.jpg" alt="123" />
                    <ImgText>
                        <h5>Kamila</h5>
                        <H6>Product manager</H6>
                    </ImgText>
                </ImgBlock>
                <ImgBlock>
                    <Img src="/avatars/3.jpg" alt="123" />
                    <ImgText>
                        <h5>Kamila</h5>
                        <H6>Product manager</H6>
                    </ImgText>
                </ImgBlock>
                <ImgBlock>
                    <Img src="/avatars/4.jpg" alt="123" />
                    <ImgText>
                        <h5>Kamila</h5>
                        <H6>Product manager</H6>
                    </ImgText>
                </ImgBlock>
                <ImgBlock>
                    <Img src="/avatars/1.jpg" alt="123" />
                    <ImgText>
                        <h5>Kamila</h5>
                        <H6>Product manager</H6>
                    </ImgText>
                </ImgBlock>
                <ImgBlock>
                    <Img src="/avatars/3.jpg" alt="123" />
                    <ImgText>
                        <h5>Kamila</h5>
                        <H6>Product manager</H6>
                    </ImgText>
                </ImgBlock>
                <ImgBlock>
                    <Img src="/avatars/5.jpg" alt="123" />
                    <ImgText>
                        <h5>Kamila</h5>
                        <H6>Product manager</H6>
                    </ImgText>
                </ImgBlock>
                <ImgBlock>
                    <Img src="/avatars/2.jpg" alt="123" />
                    <ImgText>
                        <h5>Kamila</h5>
                        <H6>Product manager</H6>
                    </ImgText>
                </ImgBlock>
                <ImgBlock>
                    <Img src="/avatars/4.jpg" alt="123" />
                    <ImgText>
                        <h5>Kamila</h5>
                        <H6>Product manager</H6>
                    </ImgText>
                </ImgBlock>
                <ImgBlock>
                    <Img src="/avatars/5.jpg" alt="123" />
                    <ImgText>
                        <h5>Kamila</h5>
                        <H6>Product manager</H6>
                    </ImgText>
                </ImgBlock>
                <ImgBlock>
                    <Img src="/avatars/1.jpg" alt="123" />
                    <ImgText>
                        <h5>Kamila</h5>
                        <H6>Product manager</H6>
                    </ImgText>
                </ImgBlock>
                <ImgBlock>
                    <Img src="/avatars/2.jpg" alt="123" />
                    <ImgText>
                        <h5>Kamila</h5>
                        <H6>Product manager</H6>
                    </ImgText>
                </ImgBlock>
                <ImgBlock>
                    <Img src="/avatars/3.jpg" alt="123" />
                    <ImgText>
                        <h5>Kamila</h5>
                        <H6>Product manager</H6>
                    </ImgText>
                </ImgBlock>
            </BodyBlock>
        </Container>
    )
}

export default Teams
