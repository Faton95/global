import React from 'react'
import styled from 'styled-components'
import {Container, Row, Col} from "reactstrap";
import { Layout } from "../UI/Layout";

const Block1 = styled.div`
    height: 500px;
    padding: 40px;
    background-color: lightgreen;
    margin-bottom: 10px;
`
const Block2 = styled.div`
    height: 245px;
    padding: 40px;
    background-color: lightgrey;
    width: 100%;
    margin-bottom: 10px;
`
const Block3 = styled.div`
    height: 245px;
    padding: 40px;
    background-color: lightgrey;
    width: 100%;
`
const Review = () => {
    return (
        <Layout>
            <Container>
                <Row>
                    <Col sm="6">
                        <Block1>
                            block 1
                        </Block1>
                    </Col>
                    <Col sm="6">
                            <Block2>
                                block 2
                            </Block2>
                            <Block3>
                                block 3
                            </Block3>
                    </Col>
                </Row>
            </Container>
        </Layout>
    )
}

export default Review
