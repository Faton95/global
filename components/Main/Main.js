import React from 'react'
import {Container, Row, Col} from 'reactstrap'
import styled from "styled-components";
import Button from '../UI/Button'
import Typewriter from 'typewriter-effect';


const Layout = styled.div`
    background-image: -webkit-gradient(linear, left top, left bottom, from(#003675), to(#031c3a));
    background-image: linear-gradient(#003675, #031c3a);
    position: relative;
    overflow: hidden;
    height: 100vh;
`
const VideoStyled = styled.video`
    height: 100%;
    width: 1750px;
    top: 0;
    left: 26%;
    -o-object-fit: cover;
    object-fit: cover;
    position: absolute;
    mix-blend-mode: screen;
    opacity: .4;
    @media (max-width: 768px){
        height: 600px;
        width: 900px;
        top: 0;
        left: 0;
    }
`

const H1 = styled.h1`
    margin-top: 15%;
    color: white;
    width: 60%;
`
const H5 = styled.h5`
    color: white;
    margin-top: 20px;
    max-width: 640px
`

const Main = () => {

    return (
        <Layout>
            <VideoStyled loop autoPlay muted>
                <source src="/videobg.mp4" type="video/mp4" />
            </VideoStyled>
            <Container>
                <H1>
                    IT Solutions for
                    <Typewriter
                        options={{
                            strings: ['daily life', 'governments', 'Global businesses'],
                            autoStart: true,
                            loop: true,
                        }}
                    />
                </H1>
                <H5>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                    labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris
                </H5>
                <br />
                <Button name={'Send Inquiry'} />
            </Container>
        </Layout>
    )
}

export default Main
