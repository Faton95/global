import React, { useState } from 'react';
import {
    Container,
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
    NavbarText
} from 'reactstrap';
import styled from 'styled-components'
import {Logo as GlobalLogo} from '../../assets/images/global-logo';
import Link from 'next/link'

const NavStyled = styled(Nav)`
    margin-left: 20%;
`
const Layout = styled.div`
    background-color: #031c3a;
    padding: 0;
    width: 100%;
`
const NavItemStyled = styled(NavLink)`
    font-size: 14px;
`
const NavbarTextStyled = styled(NavbarText)`
    margin-left: 20px;
`
const Linked = styled.p`
    text-decoration: none;
`
const Header = (props) => {
    const [isOpen, setIsOpen] = useState(false);

    const toggle = () => setIsOpen(!isOpen);

    return (
        <Layout>
            <Container>
            <Navbar dark expand="md">
                <NavbarBrand href="/"><GlobalLogo /></NavbarBrand>
                <NavbarToggler onClick={toggle} />
                <Collapse isOpen={isOpen} navbar>
                    <NavStyled className="mr-auto" navbar>
                        <NavItem active={true}>
                            <NavItemStyled href="#">
                                <Link
                                    as="/company"
                                    href="/company">
                                    <Linked>
                                        Company
                                    </Linked>
                                </Link>
                            </NavItemStyled>
                        </NavItem>
                        <NavItem>
                            <NavItemStyled href="#">
                                <Link
                                    as="/globalPay"
                                    href="/globalPay">
                                    <Linked>
                                        Global Pay
                                    </Linked>
                                </Link>
                            </NavItemStyled>
                        </NavItem>
                        <NavItem>
                            <NavItemStyled href="#">
                                <Link
                                    as="/globalID"
                                    href="/globalID">
                                    <Linked>
                                        Global ID
                                    </Linked>
                                </Link>
                            </NavItemStyled>
                        </NavItem>
                        <NavItem>
                            <NavItemStyled href="#">
                                <Link
                                    as="/globalMaps"
                                    href="/globalMaps">
                                    <Linked>
                                        Global Maps
                                    </Linked>
                                </Link>
                            </NavItemStyled>
                        </NavItem>
                        <NavItem>
                            <NavLink href="#">
                                <Link
                                    as="/globalDev"
                                    href="/globalDev">
                                    <Linked>
                                        Global Dev
                                    </Linked>
                                </Link>
                            </NavLink>
                        </NavItem>
                    </NavStyled>
                    <NavbarTextStyled>Rus</NavbarTextStyled>
                    <NavbarTextStyled>Call us</NavbarTextStyled>
                </Collapse>
            </Navbar>
            </Container>
        </Layout>
    );
}

export default Header;
