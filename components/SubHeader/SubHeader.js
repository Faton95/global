import React from 'react'
import {
    Container,
    Row,
    Col,
    Navbar,
    Nav,
    NavItem,
    NavLink
} from 'reactstrap'
import styled from 'styled-components'

const Layout = styled.div`
    background-color: #0a3f7b;
    padding-top: 5px;
`

const NavStyled = styled(Nav)`
    margin: auto;
    overflow-x: auto;
    display: flex;
    flex-wrap: nowrap;
`
const NavItemStyled = styled(NavItem)`
`
const StyledNavLink = styled(NavLink)`
    color: white !important;
    opacity: 0.5;
    border-bottom: ${props => props.active ? '2px solid rgba(255, 255, 255, 0.555)' : 'none'};
    padding: 10px;
    font-size: 14px;
    cursor: pointer;
    transition: opacity 1s;
    &:hover{
        color: white !important;
        opacity: 1;
    }
`

const SubHeader = () => {
    return (
        <Layout>
            <Container>
                <Row>
                    <NavStyled>
                        <NavItemStyled>
                            <StyledNavLink active={true}>
                                About{'\u00A0'}Us
                            </StyledNavLink>
                        </NavItemStyled>
                        <NavItemStyled>
                            <StyledNavLink>
                                Networking
                            </StyledNavLink>
                        </NavItemStyled>
                        <NavItemStyled>
                            <StyledNavLink>
                                Experience
                            </StyledNavLink>
                        </NavItemStyled>
                        <NavItemStyled>
                            <StyledNavLink>
                                Sertificate
                            </StyledNavLink>
                        </NavItemStyled>
                        <NavItemStyled>
                            <StyledNavLink>
                                Startup
                            </StyledNavLink>
                        </NavItemStyled>
                        <NavItemStyled>
                            <StyledNavLink>
                                Contacts
                            </StyledNavLink>
                        </NavItemStyled>
                    </NavStyled>
                </Row>
            </Container>
        </Layout>
    )
}

export default SubHeader
