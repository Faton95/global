import React from 'react'
import {Container, Row, Col} from "reactstrap";
import styled from "styled-components";

const Layout = styled.div`
    position: relative;
    overflow: hidden;
    padding: 80px;
    background: #031c3a;
    color: white;
`

const H4 = styled.div`
    font-size: 1.6rem;
`

const Line = styled.div`
    border-bottom: 1px solid lightgray;
    width: 100%;
    z-index: 999;
`

const ImgRotate = styled.img`
    position: absolute;
    left: -100px;
    bottom: -100px;
    height: 400px;
    width: 400px;
    animation: rotation 30s infinite linear;
    @keyframes rotation {
      from {
        transform: rotate(0deg);
      }
      to {
        transform: rotate(359deg);
      }
    }
`
const SocialBlock = styled.div`
    display: flex;
    justify-content: space-between;
`

const Footer = () => {
    return (
        <Layout>
            <ImgRotate src="/globe-transp.png" alt="image"/>
            <Container>
                <Row>
                    <Col sm="6">
                        <H4>
                            +998 71 252 37 02
                        </H4>
                        <h5>
                            +998 71 252 37 02
                        </h5>
                        <h5>
                            contact@globalsolutions.uz
                        </h5>
                        <h5>
                            39A, 2 Mirabad driveway, Tashkent city
                        </h5>
                    </Col>
                    <Col sm="3">
                        <h6>
                            About us
                        </h6>
                        <h6>
                            Certificates
                        </h6>
                        <h6>
                            Careers
                        </h6>
                        <h6>
                            Contacts
                        </h6>
                    </Col>
                    <Col sm="3">
                        <h6>
                            For Investors
                        </h6>
                        <h6>
                            Apps on Appstore
                        </h6>
                        <h6>
                            Apps on Playmarket
                        </h6>
                        <h6>
                            News and Posts
                        </h6>
                    </Col>
                </Row>
                <Row>
                    <Line />
                </Row>
                <Row>
                    <Col sm="10">
                        <h4>
                            Logo
                        </h4>
                        <h5>
                            All rights reserved Copyright 2020 ©
                        </h5>
                    </Col>
                    <Col sm="2">
                        <SocialBlock>
                            <h4>Fac</h4>
                            <h4>Ins</h4>
                            <h4>Lin</h4>
                            <h4>Twi</h4>
                        </SocialBlock>
                    </Col>
                </Row>
            </Container>
        </Layout>
    )
}

export default Footer;
