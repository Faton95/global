import React from 'react'
import styled from "styled-components";

export const Layout = styled.div`
    width: 100%;
    background-color: #031c3a;
    padding: 20px 0;
`
