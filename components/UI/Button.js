import React from 'react'
import styled from "styled-components";

const StyledButton = styled.button`
    color: #fff;
    background-color: #0d3664;
    border: none;
    border-radius: 4px;
    padding: 10px 15px;
    :hover{
        color: #fff;
        background-color: #092442;
        border-color: #071e37;
    }
`
const Button = ( {name} ) => {
    return (
        <StyledButton>
            {name}
        </StyledButton>
    )
}

export default Button
