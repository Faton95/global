// next.config.js

const withCSS = require('@zeit/next-css')
module.exports = withCSS({
    /* config options here */
})

module.exports = {
    webpack(config) {
        config.module.rules.push({
            test: /\.svg$/,
            issuer: {
                test: /\.(js|ts)x?$/,
            },
            use: ['@svgr/webpack'],
        });

        return config;
    },
};


const withImages = require('next-images')
module.exports = withImages()

const withVideos = require('next-videos')
module.exports = withVideos()
